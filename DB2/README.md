# IBM DB2 Classroom Environment
The official training environment for our postgresql classes.

## Prerequisite Tools
* Virtualbox
* Vagrant

## Instructions for Use
1. Clone this directory.
2. Run '`vagrant up`' and wait for the VM to spin up.
3. Run '`vagrant ssh`' to log into the VM.
4. Once inside the VM, execute '`/vagrant/bin/init.sh`' to spin up a local db2 instance.
5. After the previous command succeeds, execute '`/vagrant/bin/login.sh`' to log into the db2 user account.
6. To access the db2 shell, you can run '`db2`'.

## Importing the sample data
1. (DB2 Shell) Create a new database called books by running: '`CREATE DATABASE books`'
2. (DB2 Shell) Verify that your database was created by running: '`list database directory`'
3. (DB2 Shell) Run `quit` to return to the regular linux shell
4. (Linux Shell) Connect to the new books database using the following command: '`db2 connect to books user db2inst1 using password123`'
5. (Linux Shell) Execute: '`db2 -stvf /samples/create.sql`' to create all the tables necessary
6. (Linux Shell) Execute: '`db2 -stvf /samples/data.sql`' to populate the tables with data
