#!/bin/bash

# Create the Database folder
mkdir ~/database -p

# Destroy container if already running
podman rm -f mydb2 2> /dev/null

# Run the container
podman run \
	-itd \
	--name mydb2 \
	--privileged=true \
	-p 50000:50000 \
	-e LICENSE=accept \
	-e DB2INST1_PASSWORD=password123 \
	-e DBNAME=testdb \
	-v /home/vagrant/database:/database \
	-v /vagrant/samples:/samples \
	docker.io/ibmcom/db2
