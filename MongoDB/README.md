# MongoDB Classroom Environment
The official training environment for our MongoDB classes.

## Prerequisite Tools
* Virtualbox
* Vagrant

## Instructions for Use
1. Clone this directory.
2. Run '`vagrant up`' and wait for the VM to spin up.
3. Run '`vagrant ssh`' to log into the VM.
4. Run '`mongo`' to reach the mongodb prompt.
