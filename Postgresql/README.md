# Postgresql Classroom Environment
The official training environment for our postgresql classes.

## Prerequisite Tools
* Virtualbox
* Vagrant

## Instructions for Use
1. Clone this directory.
2. Run '`vagrant up`' and wait for the VM to spin up.
3. Run '`vagrant ssh`' to log into the VM.
4. Once inside the VM, switch to the postgres user by running '`sudo su postgres`'.
You'll know you did it right if the terminal prompt changes to `bash-5.0$`.
5. Run '`psql`' to reach the postgresql prompt.
